@echo OFF

if "%1"=="" goto usage
if "%2"=="" goto usage

set TIMESTAMP=%DATE:~6,4%-%DATE:~3,2%-%DATE:~0,2%-%TIME:~0,2%-%TIME:~3,2%-%TIME:~6,2%-%TIME:~9,2%
@echo TIMESTAMP=%TIMESTAMP%

set CMD=%1git.exe worktree add %2-%TIMESTAMP%
@echo CMD=%CMD%

%2git.exe worktree add %1-%TIMESTAMP%

goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This will checkout a copy of the HEAD of the current branch in to a timestamped sibling directory
@echo - see https://git-scm.com/docs/git-worktree
@echo - Please ensure you provide the root path for the new worktree and optionally the path to the git.exe
@echo -  
@echo - To configure with SourceTree 
@echo -     open Tools\Options\Custom Actions 
@echo -     click Add
@echo -     enter a Menu Caption
@echo -     enter the path to the script
@echo -     add the $REPO parameter, to provide the root path of the new worktree
@echo -     add, optionally, the path to the git.exe, if it is not available on your path.
@echo -  
@echo - Example:
@echo -  
@echo - "c:\my-repo>Add-WorkTree c:\my-repo"
@echo -  
@echo - This will checkout the HEAD of the current branch in the directory c:\my-repo-{timestamp}
@echo -  
@echo - "c:\my-repo>Add-WorkTree c:\my-repo 'c:\program files(x86)\git\cmd\'"
@echo -  
@echo - This will checkout the HEAD of the current branch in the directory c:\my-repo-{timestamp}
@echo - using the git.exe found at c:\program files(x86)\git\cmd\
@echo - a trailing \ is required

:finish